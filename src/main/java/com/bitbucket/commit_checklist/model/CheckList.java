package com.bitbucket.commit_checklist.model;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "CheckList")
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckList {

    @XmlElement(name = "CheckListType")
    private String type;
}
