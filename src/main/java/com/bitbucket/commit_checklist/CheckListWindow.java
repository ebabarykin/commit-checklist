package com.bitbucket.commit_checklist;

import com.bitbucket.commit_checklist.model.CheckList;
import com.intellij.ide.highlighter.XmlFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileEvent;
import com.intellij.openapi.vfs.VirtualFileListener;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.util.indexing.FileBasedIndex;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.Collection;

public class CheckListWindow implements ToolWindowFactory {
    private static final String CONFIG_FILENAME = "checklist.xml";

    private static VirtualFile checkListFile;
    private static Project project;
    private static ToolWindow toolWindow;

    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        CheckListWindow.project = project;
        CheckListWindow.toolWindow = toolWindow;
        VirtualFileManager.getInstance().addVirtualFileListener(new ConfigFileChangeListener());
        updateConfig();
    }

    @SneakyThrows
    private static void updateConfig() {
        if (checkListFile == null) {
            Collection<VirtualFile> xmlFiles = FileBasedIndex.getInstance()
                    .getContainingFiles(
                            FileTypeIndex.NAME,
                            XmlFileType.INSTANCE,
                            GlobalSearchScope.projectScope(project));
            xmlFiles.stream()
                    .filter(CheckListWindow::isConfigFile)
                    .findFirst()
                    .ifPresent(virtualFile -> checkListFile = virtualFile);
        }
        if (checkListFile != null) {
            JAXBContext jaxbContext = JAXBContext.newInstance(CheckList.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            CheckList checkList = (CheckList) jaxbUnmarshaller.unmarshal(checkListFile.getInputStream());
            System.out.println(checkList);
        }
    }


    private static class ConfigFileChangeListener implements VirtualFileListener {
        @Override
        public void contentsChanged(@NotNull VirtualFileEvent event) {
            VirtualFile file = event.getFile();
            if (isConfigFile(file)) {
                CheckListWindow.checkListFile = file;
                CheckListWindow.updateConfig();
            }
        }
    }

    private static boolean isConfigFile(VirtualFile file){
        return CONFIG_FILENAME.equals(file.getName()) &&
                file.getParent().equals(project.getBaseDir());
    }
}
